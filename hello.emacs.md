## Fun with Emacs

### Split Window

0. split-window-below
0. delete-other-windows
0. other-window (ctrl-x o)

### Applications

0. calendar
0. calculator
0. dired
0. list-colors-display
0. shell
0. eval-region LISP (+ 1 2)
0. irc

## Emacs Basics

### Using Menu

0. C-x, M-x

### Using in Terminal

0. emacs -nw
0. emacs <filename>
0. C-_ (undo)
0. C-g C-_ (redo)
0. C-x C-s (save)
0. C-x C-c (quit)

## Emacs Keys Basic

### Open, Save, Close, File

0. C-x C-f (find-file)
0. C-x C-s (save-buffer)
0. C-x k (kill-buffer)

### Copy, Paste, Undo

0. C-_ (undo)
0. M-w (kill-ring-save) Copy
0. C-w (kiil-region) Cut
0. C-y (yank) Cut

### Moving Cursor

0. M-b (backward-word)
0. M-f (forward-word)
0. M-< (beginning-of-buffer)
0. M-> (end-of-buffer)
0. C-f (forward-char)
0. C-b (backward-char)
0. C-v (scroll-up-command)
0. M-v (scroll-down-command)

### Deleting

0. M-d (kill-word)
0. M-<BS> (backward-kill-word)
0. C-k (kill-line)

### Select Text

0. C-<SP> (set-mark-command)
0. C-x h (mark-whole-buffer)

### Split Window

0. C-x 2 (split-window-below)
0. C-x 3 (split-window-right)
0. C-x 1 (delete-other-windows)
0. C-x o (other-window)

### Searching Text

0. C-s, C-s to next, C-r to prev, <Enter> to exit

### Find Replace Text

### Standard Copy and Paste Keys

0. cua-mode
0. C-x (cut)
0. C-c (copy)
0. C-v (paste)
0. c-z (undo)

### Everything is a Command

0. describe-function <command name>
0. describe-key <strokes>


## Advanced Tips

### Search Text

0. C-s (isearch-forward)

### Find/Replace

0. M-% (query-replace)

### Comment

0. M-; (comment-dwim)

### Add prefix to every line

0. Move to the beginning of first line
0. C-<SP> (set-mark-command)
0. Move cursor to the beginning of the last line
0. C-x r t (:string-rectangle)
0. type... <Enter>

### Delete Rectangle

0. C-x r k (kill-rectangle)

### Unprintable Charactoers

0. C-q <Tab>

### Change line endings

0. :set-buffer-file-coding-system

### Keyboard Macro

### Move thru camelCaseWords

0. :global-subword-mode

### Spell-checker

0. :flyspell-mode
0. :flyspell-buffer

## Emacs Tips

### Get Help

0. C-h k :describe-key
0. C-h f :describe-function
0. C-h a :apropos-command
0. C-h m :describe-mode
0. C-h v (describe-variable)

### Search

0. C-s C-s (search the last searched string)
0. C-s C-w (search the string under cursor)
0. :list-matching-lines
0. :delete-matching-lines
0. :delete-non-matching-lines
0. :delete-duplicate-lines
0. :highlight-phrase
0. :highlight-regexp
0. :highlight-lines-matching-regexp

### Sort Lines

0. :sort-lines
0. :reverse-region
0. :sort-fields
0. :sort-numeric-fields .e.g C-u 2 M-x sort-numberic-fields

### Spaces

0. :delete-trailing-whitespace
0. :whitespace-mode

### Clipboards

0. C-x r s (copy-to-register)
0. C-x r i (insert-register)

### Add to init file

0. :eval-region
0. :eval-buffer
0. :load-file
0. In dired, <L> :dired-to-load

### Second Shell

0. C-u (universal-argument)
0. C-u M-x shell

### Refresh File

0. :revert-buffer

### Open .info File

0. C-u M-x info C-h i <file>

### Open as hex-decimal(byte-code editor)

0. :hexl-find-file
0. :hexl-mode

### Background Color

```
(setq default-frame-alist
      '((background-color . "cornsilk")))
```

## On Windows

### List Environment

0. `gci env` in powershell
0. `set` in cmd

### Starting

0. \path\to\runemacs.exe -q --load=\path\to\init.el --load=\path\to\.emacs

### Shell

0. eshell


## Split Windows

0. C-0 (delete-window)
0. C-x ^ (enlarge-window) increase height
0. :shrink-window decrease height
0. C-x } (enlarge-window-horizontally) increase width
0. C-x { (shrink-window-horizontally)
0. C-x - (shrink-window-if-larger-than-buffer)
0. C-x + (balance-windows)

### List Buffers

0. C-x C-b (list-buffers)
0. C-x b (switch-to-buffer)
0. C-x k (kill-buffer)
0. ibuffer - colored version

### Batch Operation on Buffers

```
(defalias 'list-buffers 'ibuffer) ; make ibuffer default
```

### Switch Buffer

0. :switch-to-buffer
0. ido-mode
0. :list-buffers

### Dired

0. <Enter> (dired-find-file) open file or directory
0. q (quit-window)
0. C (dired-do-copy)
0. R (dired-do-rename)
0. D (dired-do-delete)
0. + (dired-create-directory)
0. Z (dired-do-compress)
0. m (dired-mark)
0. u (dired-unmark)
0. U (dired-unmark-all-marks)
0. %m (dired-mark-files-regexp) e.g. %m \.html$
0. g (revert-buffer)
0. ^ (dired-up-directory)
0. > (dired-next-dirline)
0. < (dired-prev-dirline)

### Bookmarks

0. C-x r m (bookmark-set)
0. C-x r l (list-bookmark)
0. C-x r b (bookmark-jump)
0. d(mark as remove),x(remove marked ad 'd'),r(rename),s(save changes)
0. :bookmark-save

### Recent File

0. :recentf-mode
0. :recentf-open-files

```
(recentf-mode 1) ; keep a list of recently opened files
```

### File Encoding

0. :revert-buffer-with-coding-system
0. :set-buffer-file-coding-system
0. :list-coding-systems
0. -*- encoding: utf-8 -*-

### Search

0. M-s . (isearch-forward-symbol-at-point)
0. M-s w (isearch-forward-word)
0. M-s _ (isearch-forward-symbol)
0. M-s o (occur) (list-matching-lines)
0. M-s h . (highlight-symbol-at-point)
0. M-s h l (highlight-lines-matching-regexp)
0. M-s h p (highlight-phrase)
0. M-s h r (highlight-regexp)
0. M-s h u (unhighlight-regexp)

### Find in Files

0. rgrep
0. lgrep
0. grep-find
0. `grep --color -nH -e "string" file`
0. :find-dired
0. :dired-do-query-replace-regexp
0. The result window: y,n,C-g,!,N

## Editing

### Copy & Paste

0. C-x r s, C-x r-i

### Completion

0. company-mode
0. C-M-i (completion-at-point)
0. hippie-expand
0. abbrev-mode : template, special characters [alpha ->]
0. ido-mode, icomplete-mode

### Jump

0. C-x C-x (exchange-point-and-mark)
0. C-u C-<SP> (move cursor to previous marked position)
0. C-x C-<SP> (move cursor to previous marked position, maybe another buffer)
0. mark-ring, global-mark-ring

```
(setq mark-ring-max 6)
(setq global-mark-ring-max 6)
(global-set-key (kbd "<f8>") 'pop-global-mark')
```

0. subword-mode
0. superword-mode : snake_case

### Wrap Lines

0. M-q (fill-paragraph)
0. :fill-region
0. C-x f (set-fill-column)
0. ruler-mode
0. visual-line-mode
0. toggle-word-wrap

### Whitespace

0. delete-trailing-whitespace
0. whitespace-cleanup
0. ^M-/r ^J-/n ^I-/t , C-q C-m/j/i


### Rectangle Edit

0. C-x r k (kill-rectangle)
0. C-x r t (replace-rectangle)
0. C-x r y (yank-rectangle)
0. C-x r N (rectangle-number-lines)
0. C-x r d (delete-rectangle)
0. C-x r c (clear-reactangle) replace with space
0. C-x r o (open-rectangle) shifting text to the right
0. C-x r M-w (copy-rectangle-as-kill)
0. C-x <SP> (rectangle-mark-mode)

### Abbreviation

0. C-q (stop abbrev from expansion)

```
(clear-abbrev-table global-abbrev-table)

(define-abbrev-table 'global-abbrev-table
  '(

    ;; net abbrev
    ("afaik" "as far as i know" )
    ("atm" "at the moment" )
    ("dfb" "difference between" )
    ("ty" "thank you" )
    ("ui" "user interface" )
    ("uns" "understand" )
    ("ur" "you are" )
    ("btw" "by the way" )
    ))
```

```
(load "~/*/my-abbrev.el")
```

### Macro

0. C-x ( (kmacro-start-macro)
0. C-x ) (kmacro-end-macro)
0. C-x e (kmacro-end-and-call-macro)
0. :call-last-kbd-macro
0. :name-last-kbd-macro
0. :insert-kbd-macro

### Unicode

0. C-x 8 '/`/^/~/" e/a/i/n/u (latin-9-prefix)

#### Input Unicode By Name

0. C-x 8 <Enter> (insert-char) then 
the name/decimal value of the Unicode
(e.g. RIGHTWARDS ARROW, #10r8594)
0. :ucs-insert in before v24.3
0. C-\ (toggle-input-method) v:current-input-method


### Align

0. :align-regexp
0. sort-regexp-fields, sort-columns

#### Space Between

0. C-u M-x align-regexp
0. .* \([0-9,]+\).*
0. -1 for group, 1 for spacing, n for repeat


### Indent

0. <Tab> (indent-for-tab-command)
0. C-M-\ (indent-region)
0. whitespace-mode


## Common Tasks

### SSH

0. :term
0. term-mode
0. M-! (shell-command)
0. :man :woman
0. C-h i (info)

### Images

0. image-mode
0. n,p
0. f,b,F for gif
0. a+,a-,a0,ar (image-*-speed)
0. image-dired (view thumbnails) named tumme before v24.x

### Web Browser

0. :eww from v24.4
0. p,r,&,g,w,v
0. :eww-open-file (open local html file)
0. :rename-buffer (open another eww)

## Org Mode

0. C-c C-e (org-export-dispatch)
0. C-c C-e h h (export the file to HTML)

### TODO

0. C-c . (insert a date like <2010-07-31 Sat>
0. C-u C-c . (insert datetime)
0. C-c C-q (insert a tag)

## HTML

0. html-mode
0. nxml-mode
0. mhtml-mode
0. C-c C-d (sgml-delete-tag)
0. C-c C-f (sgml-skip-tag-forward)
0. C-c C-b (sgml-skip-tag-backward)

### Preview in a browser

0. C-c C-v (browser-url-of-buffer)
0. C-c <Tab> (sgml-tags-invisible)