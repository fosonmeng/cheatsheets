








## 0 config & options

0. :set modelines=10, :set nomodeline
0. any-text vim:set {option}={value}... : any-text
0. :set path+=<path> - add <path> to search path


```
:scriptnames
:options
:set iskeyword& - revert to default with '&'
:set iskeyword+=$ - regard '$' as word
```


## 1 Motion

0. 2$ - line end of the 2nd line after current line
0. f{x} - cursor at next {x} char, current line
0. t{x} - cursor before nect {c} char, current line
0. F{x} - use `;` to repeat, use `,` to reverse repeat
0. T{x}
0. CTRL-E, CTRL-Y - scroll a line
0. CTRL-B, CTRL-F - scroll a screen
0. %
0. [#, ]#, [{, ]}


## 2 Deleting

0. dw - delete word
0. d$ - delete to line end
0. dd - delete current line
0. {n}dd - delete next {n} lines
0. operator [number] motion
0. 0 - move to line beginning
0. u - revert,undo
0. g-, g+, :undolist
0. U - revert operations on current line
0. CTRL-R - cancel the revert operation


## 3 Modifying

0. p - paste
0. r - replace character
0. ce - change to word end
0. c$ - change to line end
0. c [number] motion
0. x == dl
0. X == dh
0. D == d$
0. C == c$
0. s == cl
0. S == cc
0. {n}r{x}
0. . - repeat modification
0. <, > - moving shift/tab width
0. J, gJ - join lines
0. gU, gu - toggle letter (Upper/Lower)case
0. :set showmatch, :set matchtime=15 - 15\*0.1s
0. CTRL-p, CTRL-n - auto complete
0. CTRL-x CTRL-f - filename, CTRL-x CTRL-l - whole line, CTRL-x CTRL-j - tags,
CTRL-x CTRL-v - commands
0. :g/./,/^$/join - join multi lines, ':g/./', ',/^$/', 'join'
0. CTRL-a - increase number , 124


### 3.1 OMNI Complete

0. CTRL-x CTRL-o - omni complete
0. CTRL-a - repeat insert mode
0. CTRL-r

### 3.2 Abbreviate

0. :abbreviate, :iabbrev
0. :abbreviate @f fresh, :unabbreviate @f
0. :noreabbrev @a adder

### 3.2 Digraph

0. :digraph a" <char> - define a digraph

### 3.3 Align

0. :{range}center [width]
0. :{range}left [margin]

### 3.4 Table

0. :set virtualedit={empty}
0. gr, gR - virtual replace


## 4 Jump & Search

0. CTRL-G - show lineon & file info
0. G - jump to last line
0. 32G - jump to line 32 
0. gg - jump to first line
0. / - search
0. ? - reverse search
0. n - next result
0. N - prev result
0. CTRL-O - to older position
0. CTRL-I - to newer position
0. % - jump between ()[]{}
0. :s/old/new - replace once in current line
0. :s/old/new/g - replace all in current line
0. :#,#s/old/new/g - replace all between line range
0. :%s/old/new/gc - replace in all file, and need confirm
0. / ARROW\_KEYS - history searches
0. \*,\# - search word at current cursor
0. \`\` - go to last posision
0. :jumps - positioin list
0. `a - position named 'a'
0. m{x} - mark position as {x}
0. marks - ', ", [, ]
0. /const/e-1, /const/+2
0. \s - spaces, \_s - spaces and linebreak


## 5 Commands

0. !command - execute external command
0. :w FILENAME
0. v motion :w FILENAME - save selected content to file
0. :r FILENAME - read & insert content to current file
0. :r !command - insert output of command 
0. :0read, :$read - add to start/end of file
0. :.,$write <file> - write lines between . and end to <file>
0. !{motion}{program}, !5Gsort
0. :read !ls
0. :write !wc - word count 
0. CTRL-l - redraw screen
0. <TAB> - ex mode auto complete
0. :history, :history /
0. :suspend
0. :wviminfo! ~/tmp/viminfo, :rviminfo! ~/tmp/viminfo
0. :mksession sess.vim, :source sess.vim, vim -S sess.vim, sessionoptions
0. :mkview, :loadview
0. vim -e -s <file> < <commands>.vim

### 5.1 Customize

0. :command -nargs=0/1/*/?/+ Say :echo "<q-args>"
0. :command -nargs=* DoIt :call AFunction(<f-args>)
0. :command -range=% SaveIt :<line1>,<line2>write! save_file

### 5.2 Auto Commands

0. :autocmd FileWirtePre * call DateInsert()
0. :autocmd Filetype text source ~/.vim/abbrevs.vim
0. :autocmd! FileWritePre * - delete an autocmd

```
:augroup cprograms
:  autocmd ...
:augroup END

0. :autocmd FileChangeShell * nested edit
0. :set eventignore=all


## 6 Editing & Search

0. o - insert mode at next line
0. O - insert mode at prev line
0. a - insert at next char
0. A - insert at current line end
0. e - move to word end
0. y2w - copy, p - paste
0. R - replace mode, ESC - normal mode
0. search options: ic, is/incsearch, hls, noic, nohls
0. "*yy, "*p
0. is - inner sentence, as - as a sentence
0. iw - inner word, aw - as a word
0. map Q gq - `Q`(enter EX mode)
0. :?^Chapter?,/^Chapter/s=grey=gray=g - the first '?^Chapter?' search forward,
'/^Chapter/' search backward, ':s' regards '=' as dividers.
0. /Chapter/-4 - use the 4th line before the pattern
0. :[range]gobal/{pattern}/{command}


```
:set hls
:set incsearch
```

### 6.1 Folding

0. zfap - zf as paragraph, zn,zN,zi - toggle folding
0. zo/zr/zR, zc/zm/zM
0. :set viewdir=
0. :set foldmethod=expr - 


### 6.2 Tags

### 6.3 Make

0. :set makeprg=nmake -f project.mak -i % - '%' is current file name

### 6.4 Formatting

0. :%retab
0. gqap - gq a paragraph, gggqG - format whole article
0. gq]/ - formatting comments
0. :set formatoptions= - r o c
0. :set comments={flags}:{text} - like '://' 'n:>,n:!'
0. :set comments=s1:/\*,mb:\*,ex:\*/


## 7 Get Help

0. :help w, :help c_CTRL-D, :hellp insert-index, :help user-manual
0. CTRL-W - switch view pane
0. vimrc
0. command mode: CTRL-D - view completes, TAB - use a complete
0. <F1> netrw help, CTRL-J/CTRL-O jump between topics
0. :e scp://<path>/, :Explore ftp://<path>/


```
:help usr_toc.txt
:help :subject
:help :help
:help :subject
:help CTRL-B
:help i_abc
:help i_CTRL-B
:help subject()
:help -subject
:help +subject
:help digraphs.txt
:helpgrep pattern
:cn "next match
:cprev "prev match
:cN
:cfirst
:clast
:copen
:cclose
```

## 8 File and Buffer

0. ZZ - exit
0. vim -r <file> - revert file with swap
0. :e .
0. :buffers, :ls
0. :buffer 2, :b2, :sbuffer 2
0. ls | vim - - use '-' instead of file


```
:edit ~/.vimrc
filetype plugin indent on
autocmd FileType text setlocal textwidth=78
:source $VIMRUNTIME/syntax/2html.vim
:write <name>.html
```

### 8.1 Multiple Files

0. :edit! <file> - changes will lost
0. :hide edit <another file>
0. :wnext - write and next
0. :args
0. :2next
0. :set autowrite
0. :args *.txt - edit *.txt files
0. :argdo %s/\<x_cnt\>/x_counter/ge | update
0. :marks <name>
0. "{x}yas - yank sentence to storage named {x}
0. :write >> <file>
0. vim -R file, view file - read only
0. :saveas <file>
0. :file <file> - no need to wirte/save

### 8.2 windows

0. CTRL-w w - switch window
0. :close
0. :only
0. :split
0. :vsplit <file>
0. :new
0. {height}CTRL-w +, CTRL-w -
0. CTRL-w K/H/J/L - move window
0. vim -o <file1> <file2> - open split windows
0. vimdiff <file1> <file2>
0. :vertical diffsplit <file>
0. :vertical diffpatch <file>.diff
0. :set noscrollbind
0. ]c/[c - jump to difference
0. :diffupdate
0. dp/do - remove difference(diff put/diff obtain)
0. :tag/:stag <tagname>, :tags
0. CTRL-w CTRL-^, :leftabove {cmd} - split edit
0. :tabedit <file>
0. :tab split, :tab {cmd}
0. :gt - go to tab

### 8.3 GUI

0. :mkvimrc
0. :set guioptions-=T
0. "+y$, "+P - system clipboard
0. :set browsedir=
0. :browse <file>
0. :gui
0. gvim -U NONE/<rcfile>


### 8.4 Crypto

0. vim -x <file>
0. :set key={empty}, set key=<text>
0. :X
0. :setlocal noswapfile, vim -x -n <file>


### 8.5 Binary File

0. vim -b <datafile>
0. :set display=uhex
0. g CTRL-g
0. 2345go - go to 2345 byte
0. :%!xxd, :%!xxd -r - save as hex and revert
0. .Z, .gz, .bz2



## 9 Mode

0. v - visual
0. V - line
0. CTRL-v - block

### 9.1 visual mode

0. o - other side

### 9.2 Insert Mode

0. CTRL-o "g3dw - execute commands in normal mode


## 10 Plugin

0. ~/.vim/plugin/
0. ~/.vim/ftplugin/<filetype>\_<name>.vim - filetype plugin


## 11 Recording

0. q{register}...q, @{register}


## 12 Vim Script

0. let, while... endwhile
0. for i in range(1,4)...endfor
0. :let
0. let s:var = , b:var, w:var, g:var, v:var
0. :unlet s:var, :unlet! s:var, if !exists(s:var)
0. $NAME - environment var, &name - option name, @r - register name
0. if ... elseif ... else ... endif
0. a == b, >=, !=
0. str1 =~ str2, str1 !~ str2 - str1 (not) contains str2
0. a ==? b - ignore case
0. execute {commands} - execute *ex* mode commands, \\
:exe "tag " . tag_name -
use '.' to connect command and args \\
:normal gg=G - to execute normal mode commands\\
:execute "normal Inew text \<Esc>" - Insert 'new text' at the beginning of line
0. :call search('Date:', 'W')

		```
		let line = getline('.')
		let repl = substitute(line, '\a', '*', 'g')
		call setline('.',repl)
		```

0. :help functions
0. function name(args, ...) ...return val  endfunction
0. function! name(args, ...) - redefine function
0. :10,30call Count_words() - call function with a range

		```
		function Count_words() range
				let lnum = a:firstline
				while lnum <= a:lastline
		...
		endfunction
		```
0. a:1 - 1st argument, a:2 - 2nd argument
0. :function - functions have been defined
0. delfunction name
0. :help debug-scripts
0. function('AFunc') - return reference to function AFunc
0. keys(dict)
