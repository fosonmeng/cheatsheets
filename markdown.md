Markdown
------

## 简单说明和对照

### Headers

```plain
# h1
## h2
```
# h1
## h2

### Emphasis

```plain
*italic*
__bold__
```
*italic*  
__bold__

### List

```plain
* item 1
* item 2
  * item 2-1

1. item 1
2. item 2
  1. item 2-1
```
* item 1
* item 2
  * item 2-1

1. item 1
2. item 2
  1. item 2-1

### Images

```plain
![logo](assets/imgs/favicon.png)
Format: ![Alt Text](url)
```
![logo](assets/imgs/favicon.png)

### Links

```plain
[GitHub](http://github.com)
```
[GitHub](http://github.com)

### Blockquotes

```plain
As Kanye West said:

> We're living the future so
> the present is our past.
```
As Kanye West said:

> We're living the future so
> the present is our past.

### Inline code

```plain
I think you should use an `<addr>` element here instead.
```
I think you should use an `<addr>` element here instead.


### GFM(Github Flavored Markdown)

### Syntax highlighting

```plain
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```\
```
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

__支持语言：__ abnf, antlr, apl, actionscript, actionscript3, ada, ng2, apache, applescript,
awk, bnf, make, bash, bat, b3d, brainfuck, c,
cs, cpp, cf, cmake, cob, css, capnp, ceylon,
chai, cheetah, clojure, coffee, lisp, crystal,
cython, dtd, dart, diff, django/jinja, docker, ebnf,
elixir, elm, elisp, erlang, fsharp, factor, fish, forth,
fortran, gas, gdscript, glsl, genshi, gnuplot,
go, go-html-template, gradle, html, http, handlebars,
haskell, haxe, hexdump, hy, cfg, idr, io, json,
jsx, java, javascript, julia, kotlin, llvm, lighttpd,
lua, mako, mason, mysql, conf, obj-c, php, perl, pkgconfig,
eps, postgres, posh, python, R, ruby, rust, scss, sql, sass, scala, swift, toml, tcl, tex, typescript, vim,
xml, yaml, markdown, text, rst, reg

### Task Lists

```plain
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item
```
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item

### Tables

```plain
First Header                | Second Header
------------                | -------------
Content from cell 1         | Content from cell 2
Content in the first column | Content in the second column
```
First Header                | Second Header
------------                | -------------
Content from cell 1         | Content from cell 2
Content in the first column | Content in the second column

### SHA references

```plain
8c0c6d589b71b5f79072aec90b25ae4b74da28af
fosonmeng@8c0c6d589b71b5f79072aec90b25ae4b74da28af
fosonmeng/hugo@8c0c6d589b71b5f79072aec90b25ae4b74da28af
```
8c0c6d589b71b5f79072aec90b25ae4b74da28af  
fosonmeng@8c0c6d589b71b5f79072aec90b25ae4b74da28af  
fosonmeng/hugo@8c0c6d589b71b5f79072aec90b25ae4b74da28af  

### Issue references within a repository

```plain
#1
mojombo#1
mojombo/github-flavored-markdown#1
```

### Username \@mentions

```plain
@fosonmeng
```

### Automatic linking for URLs

```plain
http://www.github.com/
```


### Strikethrough

```plain
~~this~~
```
~~this~~

### Emoji

```plain
\:alien\:  
\:smile\:
```
:alien:  
:smile:

See full list at [➭ emoji-cheat-sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet/).